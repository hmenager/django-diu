from django.db import models

class AModel(models.Model):

    a_field = models.CharField('A field', max_length=50, null=True, blank=True)
