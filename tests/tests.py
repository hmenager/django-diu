import os
from io import StringIO
from unittest import mock

from django.apps import apps
from django.conf import settings
from django.core import management
from django.core.management import BaseCommand, CommandError, find_commands
from django.db import connection
from django.test import TestCase, override_settings
from django.test.utils import captured_stderr, extend_sys_path
from django.utils import translation

from .management.commands import a_command
from .models import AModel

# A minimal set of apps to avoid system checks running on all apps.
@override_settings(
    INSTALLED_APPS=[
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'tests',
    ],
)

class CommandTests(TestCase):

    def test_list_command(self):
        management.call_command('a_command', 'a')
        count = AModel.objects.count()
        self.assertEquals(count, 4)

    def test_xml_command(self):
        management.call_command('xml_command', 'b')
        count = AModel.objects.count()
        self.assertEquals(count, 4)

